
/*
 * IMPORTANT: WRITE YOUR NAME AND NetID BELOW.
 * 
 * Last Name: Booth
 * First Name: Jack
 * Netid: jlb931
 * 
 * You will do your project in this file only.
 * Do not change function delarations. However, feel free to add new functions if you want.
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>



/* Function declarations: do not change that, but you may add your own functions */
void arrange(int *, int);
void find_fibonacci(int, int);
void flipping(unsigned int);
void file_histogram(char *);
void file2upper(char *);
void file_encrypt(char *);


/* Add here function declarations of your own, if any. */



/*********************************************************************************/

/* 
 * Do  NOT change anything in main function 
 */
int main(int argc, char * argv[])
{
  int option = 0;
  int i, j;
  int * list;
  
  if(argc < 2 )
  {
     fprintf(stderr,"Usage: lab1 num [input]\n");
     fprintf(stderr,"num: 1, 2, 3, 4, 5, or 6\n");
     exit(1);
  }
  
  option = atoi(argv[1]);
  
  switch(option)
  {
    case 1: if(argc != 3)
	    {
	      fprintf(stderr,"Usage: lab1 1 num\n");
	      fprintf(stderr,"num: number of elements of the array to be arranged\n");
	      exit(1);
	    }
	    j = atoi(argv[2]);
	    list = (int *)malloc(j*sizeof(int));
	    if(!list)
	    {
	      fprintf(stderr,"Cannot allocate list in option 1\n");
	      exit(1);
	    }
	    
	    /* Gnerate random numbers in the range [0, 100) */
	    for(i = 0; i < j; i++)
	      list[i] = rand()% 100;
	    
	    /* Print the unsorted array */
	    printf("The original array:\n");
	    for(i = 0; i < j; i++) 
	      printf("%d ", list[i]);
	    printf("\n");
	    
	    printf("The arranged array:\n");
	    arrange(list, j);
	    
	    break;
	    
	    
    case 2: if(argc != 4)
	    {
	      fprintf(stderr,"Usage: lab1 2 x y\n");
	      fprintf(stderr,"x and y: positive integers and x < y and y < 1 million\n");
	      exit(1);
	    }
	    i = atoi(argv[2]);
	    j = atoi(argv[3]);
	    
	    find_fibonacci(i, j);
	    
	    break;

	    
    case 3: if(argc != 3)
	    {
	      fprintf(stderr,"Usage: lab1 3 num\n");
	      fprintf(stderr,"num: unsigned intger number\n");
	      exit(1);
	    }
	    
	    flipping( atof(argv[2]));
	    
	    break; 
	    
	    
    case 4: if(argc != 3)
	    {
	      fprintf(stderr,"Usage: lab1 4 filename\n");
	      exit(1);
	    }
	    
	    file_histogram(argv[2]);
	    
	    break;

	    
    case 5: if(argc != 3)
	    {
	      fprintf(stderr,"Usage: lab1 5 filename\n");
	      exit(1);
	    } 
	    
	    file2upper(argv[2]);
	    
	    break;

	    
    case 6: if(argc != 3)
	    {
	      fprintf(stderr,"Usage: lab1 46filename\n");
	      exit(1);
	    }
	    
	    file_encrypt(argv[2]);
	    
	    break;	      
	    
    default: fprintf(stderr, "You entered an invalid option!\n");
	     exit(1);
  }
  
  return 0;
}

/******* Start filling the blanks from here and add any extra functions you want, if any *****/

/*
 * Part 1:
 * In this function, you are given two inputs:
 * inputs: 
 * 	an array of int list[] 
 * 	The number of elements in that array: num
 * You need to put the even numbers first (if any), sorted in ascending order, followed
 * by the odd numbers (if any) sorted in ascending order too.
 * Example: a list of 5 elements: 5 4 3 2 1 
 * The output will be: 2 4 1 3 5 
 * Finally, print the array on the screen.
 * */
void arrange(int *list, int num)
{
  int temp = 0;
  int evenNum = 0;
  int oddNum = 0;
  int evenPlace = 0;
  int oddPlace = 0;

//count the number of even and odd numbers in the array
  for(int i = 0; i<num; i++)
  {
	if(list[i]%2==0)
	{
		evenNum++;
	}
	else
	{
		oddNum++;
	}	

  }

//create separate even and odd arrays
  int evens[evenNum];
  int odds[oddNum];

//iterate through the list array and separate the even and odd numbers into
//their respective arrays
  for(int i = 0; i<num; i++)
  {
	if(list[i]%2==0)
	{
		evens[evenPlace] = list[i];
		evenPlace++;
	}
	else
	{
		odds[oddPlace] = list[i];
		oddPlace++;
	}

  }

//sort the evens array in ascending order
  for(int i = 0; i<evenNum; i++)
  {
	for(int j = i+1; j<evenNum; j++)
	{
		if(evens[i] > evens[j])
		{
			temp = evens[i];
			evens[i] = evens[j];
			evens[j] = temp;
		}
	}
  }

//sort the odds array in ascending order
  for(int i = 0; i<oddNum; i++)
  {
	for(int j = i+1; j<oddNum; j++)
	{
		if(odds[i] > odds[j])
		{
			temp = odds[i];
			odds[i] = odds[j];
			odds[j] = temp;
		}
	}
  }

//iterate through the evens array and print out its values
  for(int i = 0; i<evenNum; i++)
  {
	printf("%d ", evens[i]);
  }

//iterate through the odds array and print out its values
  for(int i = 0; i<oddNum; i++)
  {
	printf("%d ", odds[i]);
  }


}


/*********************************************************************************/

/* 
 * Part 2:
 * In this part, you are given two positive integer, x and y, larger than 0.
 * You need to print all the Fibonacci numbers between x and y (NOT including x and y themselves), if any.
 */
void find_fibonacci(int x, int y)
{
//initializing useful variables
  int firstCheck, secondCheck, temp1=0, temp2=0, a, b;

//iterate through the numbers from x+1 to y-1 and check if they are fibonacci
  for(int i = x+1; i<y; i++)
  {
//you can check if a number is in the fibonacci sequence by checking if
//5i^2 + 4 or 5i^2 - 4 is a perfect square

//first set up each of these equations
	firstCheck = (5*i*i + 4);
	secondCheck = (5*i*i - 4);

//if the first one is a perfect square, we will print our current iteration
	for(a = 0; a <= firstCheck; a++)
	{
		if(firstCheck == (a*a))
			temp1 = 1;
	}

//if the seconds is a perfect square, also print out current iteration
	for(b = 0; b<=secondCheck; b++)
	{
		if(secondCheck == (b*b))
			temp2 = 1;
	}

	if(temp1 == 1 || temp2 == 1)
	{
		printf("%d ", i);
	}
//reset variables for next iteration
	temp1=temp2=0;
  }  
  
  
}


/*********************************************************************************/

/*
 * Part 3:
 * Given an unsigned integer (so has values of 0 and up), print on the screen 
 * the flipping of that number.
 * That is, if the number given is: 1234  then you print: 4321
 */
void flipping(unsigned int num)
{
  int reverse = 0;
  while(num!=0)
  {
//simple formula to reverse int: use modulus to get the last place, add it to
//reverse, then take out the last place of the original int using division
//repeat until num is 0
	reverse = reverse*10;
	reverse = reverse + num%10;
	num =num/10;
  }  
  printf("%d ", reverse);
    
 
  
}

/*********************************************************************************/

/*
 * Part 4
 * The input is a filename.
 * The filename contains a string of characters (can be upper case letters, lower case letters, numbers, ...).
 * The output is a histogram of the file printed on the screen ONLY for lower case letters.
 * Example: 
 * The input file in.txt contains:  a1aaAbBBb709bbc
 * The output will be:
 * a: 3
 * b: 4
 * c: 1
 * .... and so one
 * The file can contain any characters, not necessarily only lower case characters.
 */
void file_histogram(char *filename)
{
  char *buffer = NULL;
  size_t size = 0;
  FILE *fptr1;
//open the file as read only
  fptr1 = fopen(filename, "r");
//go to the end of the file and set size to how many bytes we passed
  fseek(fptr1, 0, SEEK_END);
  size = ftell(fptr1);
//go back to beginning of file
  rewind(fptr1);
//allocate buffer size
  buffer = malloc((size+1) * sizeof(*buffer));

//read file into buffer
  fread(buffer, size, 1, fptr1);

  buffer[size] = '\0';

//  printf("%s\n", buffer); 
  
  int count[26] = {0};
  for(int i = 0; buffer[i] != '\0'; i++)
  {
//only deal wiht lowercase characters
	if(buffer[i] >= 'a' && buffer[i] <= 'z')
	{
//increment count for the character we are on
		char c = buffer[i];
		count[(int)(c) - 'a']++;	
	}	
  }

//print out the count for all lowercase letters
  for(int i = 0; i<26; i++)
  {
	printf("%c: %2d\n", i + 'a', count[i]); 
  }
  
  free(buffer);
  fclose(fptr1);  
  
}

/*********************************************************************************/

/* 
 * Part 5:
 * Input: filename
 * Output:filenameCAPITAL
 * The output files contains the same characters as the input file but in upper case.
 * The input file will ONLY contain lower case letters and no spaces.
 * If the input filename is: mmmm 
 * The output filename is: mmmmCAPITAL
 */
void file2upper(char *filename)
{
//some variable declarations
  char *buffer = NULL;
  size_t size = 0;  
  char *name= filename;
  char *extension = "CAPITAL";
  char *finalName;

//creating the new filename
//allocate for the final name the size of the original + size of addon (CAPITAL)
  finalName = malloc(strlen(name)+1+7);
//copy name onto finalName
  strcpy(finalName, name);
//add CAPITAL to the end
  strcat(finalName, extension);
  FILE *fptr1, *fptr2;

//file read reused from part 4
  fptr1 = fopen(filename, "r");
  fseek(fptr1, 0, SEEK_END);
  size = ftell(fptr1);
  rewind(fptr1);
  buffer = malloc((size+1) * sizeof(*buffer));

  fread(buffer, size, 1, fptr1);

  buffer[size] = '\0';

//create new file using previously decided file name
  fptr2 = fopen(finalName, "w");

//iterate through buffer and make it capital  
  for(int i = 0; i<strlen(buffer); i++)
  {
//	buffer[i] = toupper(buffer[i]);
	buffer[i] = buffer[i] - ('a'-'A');
//	printf("%d ", buffer[i]);
  } 
//write capital letters to the file
  fprintf(fptr2, "%s\n", buffer);

//freeing memory and closing files
  free(finalName);
  free(buffer);
  fclose(fptr1);
  fclose(fptr2);  
  
}

/*********************************************************************************/

/*
 * Part 6:
 * In that last part, you will learn to implement a very simple encryption.
 * Given a file that contains a series of lower case charaters (may be separated by white space),
 * replace all non-white space with the lower case letter that exists 3 letters before, in a circular way.
 * For example: 
 * e will be replaced with b
 * d will be replaced with a 
 * c will be replaced with z  <--- circular
 * b will be replaced with y  <--- circular
 * and white spaces will be left unchanged.
 * Print the output on the screen.
 */
void file_encrypt(char * filename)
{
  char *buffer = NULL;
  size_t size = 0;
  FILE *fptr1;

//reuse file read from part 4
  fptr1 = fopen(filename, "r");
  fseek(fptr1, 0, SEEK_END);
  size = ftell(fptr1);
  rewind(fptr1);
  buffer = malloc((size+1) * sizeof(*buffer));

  fread(buffer, size, 1, fptr1);

  buffer[size] = '\0';

//iterate through buffer and encrypt each character
  for(int i = 0; buffer[i] != '\0'; i++)
  {
	if(isalpha(buffer[i]))
	{
//if character is at beginning of alphabet, move it towards the appropriate 
//letter at the end of the alphabet, making it circular
//otherwise, move it back three characters
		if(buffer[i] == 'a')
			buffer[i] = 'x';
		else if(buffer[i] == 'b')
			buffer[i] = 'y';
		else if(buffer[i] == 'c')
			buffer[i] = 'z';
		else
			buffer[i] = buffer[i] - 3;
	}
	printf("%c", buffer[i]);
  }  
  printf("\n");
  free(buffer);
  fclose(fptr1);
  
}
